package ru.vlsu.isse.parprog.gol;

import ru.vlsu.isse.parprog.gol.model.Field;

public interface GameStrategy
{
    void updateField(Field source, Field dest, int offset, int length);
}
