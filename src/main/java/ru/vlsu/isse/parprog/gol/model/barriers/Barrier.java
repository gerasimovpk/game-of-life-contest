package ru.vlsu.isse.parprog.gol.model.barriers;

public interface Barrier
{
    void with(int index, Runnable runnable);
}