package ru.vlsu.isse.parprog.gol.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ClosedFieldTest
{
    @Test
    public void testIsAlive()
    {
        ClosedField field = new ClosedField(4, 2);
        
        field.set(0, 1, true);
        field.set(0, 2, true);
        
        field.set(1, 0, true);
        field.set(1, 3, true);
        
        System.out.println(field);
        
        assertTrue(field.isAlive(-1, -1));
        assertFalse(field.isAlive(0, -1));
        assertTrue(field.isAlive(-1, 0));
        
        assertFalse(field.isAlive(-2, 0));
        assertTrue(field.isAlive(-3, 0));
        assertFalse(field.isAlive(-4, 0));
        assertTrue(field.isAlive(0, -3));
        assertFalse(field.isAlive(0, -4));
        assertFalse(field.isAlive(0, -5));
        assertTrue(field.isAlive(0, -6));
        assertFalse(field.isAlive(0, -8));
        
        assertFalse(field.isAlive(0, 4));
        assertFalse(field.isAlive(0, 8));
        
        assertTrue(field.isAlive(0, 5));
        assertTrue(field.isAlive(0, 6));
        assertFalse(field.isAlive(0, 7));
        
        assertFalse(field.isAlive(2, 0));
        assertTrue(field.isAlive(3, 0));
        assertFalse(field.isAlive(4, 0));
        assertTrue(field.isAlive(5, 0));
    }
}
