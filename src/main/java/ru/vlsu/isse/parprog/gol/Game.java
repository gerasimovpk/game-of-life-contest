package ru.vlsu.isse.parprog.gol;

import org.apache.commons.lang3.time.StopWatch;

import ru.vlsu.isse.parprog.gol.model.ClosedField;
import ru.vlsu.isse.parprog.gol.model.Field;

public class Game
{
    private Field field;
    private final GameStrategy strategy;
    
    public Game(ClosedField field, GameStrategy strategy)
    {
        this.field = field;
        this.strategy = strategy;
    }
    
    public void update(int numberOfEpochs)
    {
        Field source = field;
        Field dest = new ClosedField(field);
        
        StopWatch stopWatch = new StopWatch();
        
        for (int i = 0; i < numberOfEpochs; i++)
        {
            if (i == 1 || numberOfEpochs == 1)
            {
                stopWatch.start();
            }
            
            strategy.updateField(source, dest, 0, source.size());
            
            Field temp = source;
            source = dest;
            dest = temp;
            
            System.out.println("Completed iteration #" + i + ": " + stopWatch);
        }
        
        stopWatch.stop();
        
        System.out.println("Total time for "
                + (numberOfEpochs == 1 ? 1 : numberOfEpochs - 1)
                + " iterations: " + stopWatch);
        
        field = source;
    }
    
    public Field getField()
    {
        return field;
    }
}
