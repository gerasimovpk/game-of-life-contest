package ru.vlsu.isse.parprog.gol;

import static ru.vlsu.isse.parprog.gol.model.Field.loadFromFile;

import java.io.File;
import java.io.IOException;

import ru.vlsu.isse.parprog.gol.model.ClosedField;

public class Main
{

    public static void main(String[] args) throws IOException
    {
        if (args.length != 4)
        {
            printUsage();
            System.exit(1);
        }
        
        int numberOfEpochs = Integer.parseInt(args[2]);
        
        GameStrategy strategy = "s".equals(args[3])
            ? new SequentialStrategy()
            : new ParallelStrategy(1000000);
        
        Game game = new Game(new ClosedField(loadFromFile(new File(args[0]))), strategy);
        
        game.update(numberOfEpochs);
        
        game.getField().saveToFile(new File(args[1]));
    }

    private static void printUsage()
    {
        System.out.println("usage: java "
                + Main.class.getName() + " <inputFile> <outputFile> <numberOfEpochs> <s|p>");
    }

}
